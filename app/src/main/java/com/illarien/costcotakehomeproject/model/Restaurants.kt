package com.illarien.costcotakehomeproject.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Entity
data class Restaurants (
    @PrimaryKey
    val city: String = "",

    @SerializedName("total_entries")
    @Expose
    val totalEntries: Int = 0,

    @SerializedName("per_page")
    @Expose
    val perPage: Int = 1,

    @SerializedName("current_page")
    @Expose
    val currentPage: Int = 1,

    @SerializedName("restaurants")
    @Expose
    val restaurants: List<Restaurant> = emptyList()
)