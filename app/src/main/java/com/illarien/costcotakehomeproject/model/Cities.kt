package com.illarien.costcotakehomeproject.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Entity
data class Cities (
    @PrimaryKey
    val id: Int = 1,

    @SerializedName("count")
    @Expose
    val count: Int = 0,

    @SerializedName("cities")
    @Expose
    val cities: List<String> = emptyList()
)