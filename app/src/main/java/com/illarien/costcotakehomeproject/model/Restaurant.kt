package com.illarien.costcotakehomeproject.model

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Restaurant (
    @SerializedName("id")
    @Expose
    val id: Int = 0,

    @SerializedName("name")
    @Expose
    val name: String = "",

    @SerializedName("address")
    @Expose
    val address: String = "",

    @SerializedName("city")
    @Expose
    val city: String = "",

    @SerializedName("state")
    @Expose
    val state: String = "",

    @SerializedName("area")
    @Expose
    val area: String = "",

    @SerializedName("postal_code")
    @Expose
    val postalCode: String = "",

    @SerializedName("country")
    @Expose
    val country: String = "",

    @SerializedName("phone")
    @Expose
    val phone: String = "",

    @SerializedName("lat")
    @Expose
    val lat: Double = 0.0,

    @SerializedName("lng")
    @Expose
    val lng: Double = 0.0,

    @SerializedName("price")
    @Expose
    val price: Int = 0,

    @SerializedName("reserve_url")
    @Expose
    val reserveUrl: String = "",

    @SerializedName("mobile_reserve_url")
    @Expose
    val mobileReserveUrl: String = "",

    @SerializedName("image_url")
    @Expose
    val imageUrl: String = ""
) : Parcelable