package com.illarien.costcotakehomeproject.di

import android.app.Application
import com.google.gson.GsonBuilder
import com.illarien.costcotakehomeproject.repository.api.OpenTableApi
import com.illarien.costcotakehomeproject.utils.Constants.API_URL
import com.illarien.costcotakehomeproject.utils.InternetBroadcastReceiver
import com.illarien.costcotakehomeproject.utils.InternetBroadcastReceiverImpl
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
object NetworkModule {
    @Provides @JvmStatic
    fun provideGsonConverterFactory(): GsonConverterFactory =
        GsonConverterFactory.create(GsonBuilder().setLenient().create())

    @Provides @JvmStatic
    fun provideOkHttpClient(): OkHttpClient {
        /**
         * Disable loggingInterceptor if you do not need retrofit logging
         */
        val loggingInterceptor = HttpLoggingInterceptor()
            .apply { level = HttpLoggingInterceptor.Level.BODY }

        return OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .connectTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .build()
    }

    @Provides @JvmStatic
    fun provideRetrofit(
        client: OkHttpClient,
        gsonConverterFactory: GsonConverterFactory
    ): Retrofit =
        Retrofit.Builder()
            .baseUrl(API_URL)
            .client(client)
            .addConverterFactory(gsonConverterFactory)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()

    @Provides @Singleton @JvmStatic
    fun provideOpenTableApi(retrofit: Retrofit): OpenTableApi =
        retrofit.create(OpenTableApi::class.java)

    @Provides @Singleton @JvmStatic
    fun provideInternetBroadcastReceiver(application: Application): InternetBroadcastReceiver =
        InternetBroadcastReceiverImpl(application)
}