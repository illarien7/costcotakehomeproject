package com.illarien.costcotakehomeproject.di

import android.app.Application
import com.illarien.costcotakehomeproject.repository.OpenTableDataBase
import com.illarien.costcotakehomeproject.repository.OpenTableRepository
import com.illarien.costcotakehomeproject.ui.main.MainActivity
import com.illarien.costcotakehomeproject.utils.InternetBroadcastReceiver
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        NetworkModule::class,
        RoomModule::class,
        RepositoryModule::class
    ]
)
interface AppComponent {

    fun application(): Application

    fun inject(mainActivity: MainActivity)

    fun database(): OpenTableDataBase
    fun repository(): OpenTableRepository
    fun internetBroadcastReceiver(): InternetBroadcastReceiver

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder
        fun build(): AppComponent
    }
}