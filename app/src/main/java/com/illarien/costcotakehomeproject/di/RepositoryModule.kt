package com.illarien.costcotakehomeproject.di

import com.illarien.costcotakehomeproject.repository.*
import com.illarien.costcotakehomeproject.repository.api.OpenTableApi
import com.illarien.costcotakehomeproject.repository.dao.CityDao
import com.illarien.costcotakehomeproject.repository.dao.RestaurantDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
object RepositoryModule {
    @Singleton @Provides @JvmStatic
    fun openTableRepository(
        openTableApi: OpenTableApi,
        cityDao: CityDao,
        restaurantDao: RestaurantDao
    ): OpenTableRepository = OpenTableRepositoryImpl(openTableApi, cityDao, restaurantDao)
}