package com.illarien.costcotakehomeproject.di

import android.app.Application
import androidx.room.Room
import com.illarien.costcotakehomeproject.repository.dao.CityDao
import com.illarien.costcotakehomeproject.repository.OpenTableDataBase
import com.illarien.costcotakehomeproject.repository.dao.RestaurantDao
import com.illarien.costcotakehomeproject.utils.Constants.DB_NAME
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
object RoomModule {
    @Singleton @Provides @JvmStatic
    fun provideDatabase(application: Application): OpenTableDataBase =
        Room.databaseBuilder(application, OpenTableDataBase::class.java, DB_NAME)
            .fallbackToDestructiveMigration()
            .build()

    @Singleton @Provides @JvmStatic
    fun provideCityDao(database: OpenTableDataBase): CityDao = database.cityDao()

    @Singleton @Provides @JvmStatic
    fun provideRestaurantDao(database: OpenTableDataBase): RestaurantDao = database.restaurantDao()
}