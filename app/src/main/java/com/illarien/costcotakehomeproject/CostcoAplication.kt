package com.illarien.costcotakehomeproject

import android.app.Application
import com.illarien.costcotakehomeproject.di.AppComponent
import com.illarien.costcotakehomeproject.di.DaggerAppComponent

class CostcoApplication : Application() {
    companion object {
        lateinit var component: AppComponent
    }

    override fun onCreate() {
        super.onCreate()

        component = DaggerAppComponent.builder()
            .application(this)
            .build()
    }
}