package com.illarien.costcotakehomeproject.ui.main

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import com.illarien.costcotakehomeproject.model.Cities
import com.illarien.costcotakehomeproject.model.Restaurant
import com.illarien.costcotakehomeproject.repository.OpenTableRepository
import com.illarien.costcotakehomeproject.ui.main.viewstate.MainViewState
import com.illarien.costcotakehomeproject.utils.DisposableManager
import com.illarien.costcotakehomeproject.utils.InternetBroadcastReceiver
import javax.inject.Inject

class MainViewModel @Inject constructor(
    application: Application,
    private val openTableRepository: OpenTableRepository,
    private val broadcastReceiver: InternetBroadcastReceiver
) : AndroidViewModel(application) {

    private val mainState = MutableLiveData<MainState>()
    private val cities = openTableRepository.getCities()
    private var restaurants = openTableRepository.getAllRestaurants()

    var mainViewState: LiveData<MainViewState> =
        MediatorLiveData<MainViewState>().apply {
            value =
                MainViewState()

            var mState: MainState = MainState.Loading
            var mCities = Cities()
            var mRestaurants: List<Restaurant> = emptyList()
            var mSelectedCity = "Search here:"

            fun update() {
                value =
                    MainViewState(
                        state = mState,
                        cities = mCities,
                        restaurants = mRestaurants,
                        selectedCity = mSelectedCity
                    )
            }

            addSource(mainState) {
                mState = it
                update()
            }

            addSource(cities) {
                it?.let {
                    mCities = it
                    update()
                }
            }

            addSource(restaurants) {
                it?.let {
                    mRestaurants = it.restaurants
                    mSelectedCity = it.city
                    update()
                }
            }
        }

    init {
        broadcastReceiver.registerBroadCastReceiver { mainState.postValue(it) }
    }

    fun fetchCities() {
        openTableRepository.fetchCities { mainState.postValue(it) }
    }

    fun fetchRestaurants(city: String) {
        mainState.postValue(MainState.Loading)
        openTableRepository.fetchRestaurantByCity(city) { mainState.postValue(it) }
    }

    fun onDestroy() {
        DisposableManager.clearDisposables()
        broadcastReceiver.unRegisterBroadCastReceiver()
    }
}