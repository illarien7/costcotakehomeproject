package com.illarien.costcotakehomeproject.ui.main.recyclerview

import android.app.Activity
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.illarien.costcotakehomeproject.R
import com.illarien.costcotakehomeproject.model.Restaurant

class RestaurantAdapter (
    private val activity: Activity
): RecyclerView.Adapter<RestaurantHolder>() {

    private var restaurants = ArrayList<Restaurant>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RestaurantHolder {
        val view = activity.layoutInflater
            .inflate(R.layout.recycler_view_item, parent, false)
        return RestaurantHolder(view)
    }

    override fun onBindViewHolder(holder: RestaurantHolder, position: Int) {
        holder.bind(restaurants[position])
    }

    override fun getItemCount() = restaurants.size

    fun setData(data: List<Restaurant>) {
        restaurants.clear()
        restaurants.addAll(data)
        notifyDataSetChanged()
    }

    fun getRestaurantByPosition(position: Int): Restaurant =
        when (position in 0..restaurants.size) {
            true -> restaurants[position]
            else -> Restaurant()
        }
}