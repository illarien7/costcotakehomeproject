package com.illarien.costcotakehomeproject.ui.main.recyclerview;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.illarien.costcotakehomeproject.R;
import com.illarien.costcotakehomeproject.model.Restaurant;
import com.squareup.picasso.Picasso;

public class RestaurantHolder extends RecyclerView.ViewHolder {

    private TextView name;
    private ImageView image;

    public RestaurantHolder (View itemView) {
        super(itemView);
        this.name = itemView.findViewById(R.id.name);
        this.image = (ImageView) itemView.findViewById(R.id.image);
    }

    public void bind(Restaurant restaurant) {
        name.setText(restaurant.getName());

        Picasso.get()
            .load(restaurant.getImageUrl())
            .placeholder(R.drawable.ic_aspect_ratio_black_24dp)
            .resize(150, 100)
            .centerInside()
            .into(image);
    }
}
