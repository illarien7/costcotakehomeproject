package com.illarien.costcotakehomeproject.ui.main.viewstate

import com.illarien.costcotakehomeproject.model.Cities
import com.illarien.costcotakehomeproject.model.Restaurant
import com.illarien.costcotakehomeproject.ui.main.MainState

class MainViewState (
    val state: MainState = MainState.Loading,
    val cities: Cities = Cities(),
    val restaurants: List<Restaurant> = emptyList(),
    val selectedCity: String = ""
)