package com.illarien.costcotakehomeproject.ui.details;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.illarien.costcotakehomeproject.R;
import com.illarien.costcotakehomeproject.model.Restaurant;
import com.squareup.picasso.Picasso;

import static com.illarien.costcotakehomeproject.utils.Constants.RESTAURANT;

public class DetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        Restaurant restaurant = getIntent().getParcelableExtra(RESTAURANT);
        setView(restaurant);
    }

    private void setView(Restaurant restaurant) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(restaurant.getName());
        }

        ((TextView) findViewById(R.id.name)).setText(restaurant.getName());
        ((TextView) findViewById(R.id.address)).setText(restaurant.getAddress());
        ((TextView) findViewById(R.id.city)).setText(restaurant.getCity());
        ((TextView) findViewById(R.id.state)).setText(restaurant.getState());
        ((TextView) findViewById(R.id.area)).setText(restaurant.getArea());
        ((TextView) findViewById(R.id.postal_code)).setText(restaurant.getPostalCode());
        ((TextView) findViewById(R.id.country)).setText(restaurant.getCountry());
        ((TextView) findViewById(R.id.phone)).setText(restaurant.getPhone());

        Picasso.get()
                .load(restaurant.getImageUrl())
                .placeholder(R.drawable.ic_aspect_ratio_black_24dp)
                .resize(400, 300)
                .centerInside()
                .into((ImageView) findViewById(R.id.image));
    }
}
