package com.illarien.costcotakehomeproject.ui.main

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.illarien.costcotakehomeproject.CostcoApplication
import com.illarien.costcotakehomeproject.R
import com.illarien.costcotakehomeproject.ui.*
import com.illarien.costcotakehomeproject.ui.details.DetailsActivity
import com.illarien.costcotakehomeproject.ui.main.recyclerview.RestaurantAdapter
import com.illarien.costcotakehomeproject.ui.main.viewstate.MainViewState
import com.illarien.costcotakehomeproject.utils.Constants.RESTAURANT
import com.illarien.costcotakehomeproject.utils.ViewModelFactory
import com.illarien.costcotakehomeproject.utils.snackbar
import com.miguelcatalan.materialsearchview.MaterialSearchView
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject


class MainActivity : AppCompatActivity() {

    @Inject lateinit var viewModelFactory: ViewModelFactory<MainViewModel>
    private lateinit var viewModel: MainViewModel
    private lateinit var adapter: RestaurantAdapter
    private var sb: Snackbar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbar))
        supportActionBar?.title = getString(R.string.search_here)

        init()

        viewModel.mainViewState
            .observe(this, Observer {
                when (it.state) {
                    is MainState.InternetConnectionOff -> onInternetConnectionOff()
                    is MainState.InternetConnectionOn -> onInternetConnectionOn()
                    is MainState.Loading -> onLoading()
                    is MainState.OnSuccess -> onSuccess(it)
                    is MainState.OnRestaurantsLoaded -> onSuccess(it)
                    is MainState.OnError -> onError()
                }
            })
    }

    override fun onDestroy() {
        viewModel.onDestroy()
        super.onDestroy()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        val item: MenuItem = menu.findItem(R.id.action_search)
        search_view.setMenuItem(item)
        return true
    }

    private fun init() {
        CostcoApplication.component.inject(this)

        recyclerView.layoutManager = LinearLayoutManager(this)
        adapter = RestaurantAdapter(this)
        recyclerView.adapter = adapter

        viewModel = ViewModelProvider(this, viewModelFactory)
            .get(MainViewModel::class.java)

        /**
         * Material Search View Listener
         */
        search_view.setOnQueryTextListener(object : MaterialSearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(city: String): Boolean {
                viewModel.fetchRestaurants(city)
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean = false
        })

        search_view.setOnSearchViewListener(object : MaterialSearchView.SearchViewListener {
            override fun onSearchViewShown() {
                recyclerView.visibility = View.GONE
            }

            override fun onSearchViewClosed() {
                recyclerView.visibility = View.VISIBLE
            }
        })


        /**
         * Recycler view touch listener
         */
        recyclerView.addOnItemTouchListener(
            RecyclerTouchListener(
                this,
                object :
                    RecyclerClickListener {
                    override fun onClick(view: View?, position: Int) {
                        openRestaurantDetails(position)
                    }
                })
        )
    }

    /**
     * Helper methods
     */
    private fun onInternetConnectionOff() {
        sb = snackbar(getString(R.string.no_internet_connection))
        main_view.visibility = View.GONE
        progress.visibility = View.GONE
    }

    private fun onInternetConnectionOn() {
        sb?.dismiss()
        viewModel.fetchCities()
    }

    private fun onLoading() {
        main_view.visibility = View.GONE
        progress.visibility = View.VISIBLE
    }

    private fun onSuccess(state: MainViewState) {
        if (state.cities.cities.isNotEmpty()) {
            search_view.setSuggestions(state.cities.cities.toTypedArray())
        }

        progress.visibility = View.GONE
        main_view.visibility = View.VISIBLE

        updateView(state)
    }

    private fun onError() {
        snackbar(getString(R.string.something_went_wrong))
    }

    private fun updateView(state: MainViewState) {
        supportActionBar?.title = state.selectedCity
        recyclerView.stopScroll()
        adapter.setData(state.restaurants)
    }

    private fun openRestaurantDetails(position: Int) {
        val intent = Intent(this, DetailsActivity::class.java)
            .putExtra(RESTAURANT, adapter.getRestaurantByPosition(position))
        startActivity(intent)
    }
}