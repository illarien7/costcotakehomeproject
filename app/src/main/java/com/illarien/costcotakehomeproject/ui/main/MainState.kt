package com.illarien.costcotakehomeproject.ui.main

import com.illarien.costcotakehomeproject.model.Restaurant

sealed class MainState {
    object InternetConnectionOn : MainState()
    object InternetConnectionOff : MainState()
    object Loading : MainState()
    object OnSuccess : MainState()
    data class OnRestaurantsLoaded(val data: List<Restaurant>) : MainState()
    data class OnError(val error: Throwable) : MainState()
}