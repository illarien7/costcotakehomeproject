package com.illarien.costcotakehomeproject.utils

import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Extension function to perform db and other operations asynchronous
 */
fun invokeRx(runnable: () -> Unit) {
    Completable.fromCallable { runnable() }
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe()
        .attach()
}