package com.illarien.costcotakehomeproject.utils

object Constants {
    const val DB_NAME = "open_table_db"
    const val API_URL = "https://opentable.herokuapp.com/api/"
    const val RESTAURANT = "Restaurant"
}