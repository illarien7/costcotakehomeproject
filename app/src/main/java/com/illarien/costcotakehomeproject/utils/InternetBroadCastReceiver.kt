package com.illarien.costcotakehomeproject.utils

import android.app.Application
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.NetworkInfo
import com.illarien.costcotakehomeproject.ui.main.MainState
import javax.inject.Inject

/**
 * InternetBroadcastReceiver interface and implementation
 * to track Internet connection availability
 */
interface InternetBroadcastReceiver {
    fun registerBroadCastReceiver(callback: (state: MainState) -> Unit)
    fun unRegisterBroadCastReceiver()
}

class InternetBroadcastReceiverImpl @Inject constructor(
    private val application: Application
) : InternetBroadcastReceiver {

    companion object {
        var broadcastReceiver: BroadcastReceiver? = null
    }

    override fun registerBroadCastReceiver(callback: (state: MainState) -> Unit) {
        if (broadcastReceiver == null) {
            val filter = IntentFilter()
            filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION)

            broadcastReceiver = object : BroadcastReceiver() {
                override fun onReceive(_context: Context, intent: Intent) {
                    val info = intent.extras.getParcelable<NetworkInfo>("networkInfo")

                    when (info.state == NetworkInfo.State.CONNECTED) {
                        true -> callback(MainState.InternetConnectionOn)
                        false -> callback(MainState.InternetConnectionOff)
                    }
                }
            }

            application.registerReceiver(broadcastReceiver, filter)
        }
    }

    override fun unRegisterBroadCastReceiver() {
        broadcastReceiver?.let {
            application.unregisterReceiver(it)
            broadcastReceiver = null
        }
    }
}