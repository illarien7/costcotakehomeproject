package com.illarien.costcotakehomeproject.utils

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

/**
 * Disposable Manager to properly handle disposables lifecycle
 */
object DisposableManager {
    val disposables = CompositeDisposable()

    fun clearDisposables() = disposables.clear()
}

fun Disposable.attach() = DisposableManager.disposables.add(this)