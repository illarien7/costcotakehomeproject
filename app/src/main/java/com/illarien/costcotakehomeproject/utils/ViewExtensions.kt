package com.illarien.costcotakehomeproject.utils

import android.app.Activity
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar

/**
 * Extension function to display snackbar
 */
fun Activity.snackbar(message: String): Snackbar {
    val sb = Snackbar.make(findViewById(android.R.id.content), message, Snackbar.LENGTH_INDEFINITE)
    sb.view.setBackgroundColor(ContextCompat.getColor(this, android.R.color.holo_red_light))
    sb.show()
    return sb
}