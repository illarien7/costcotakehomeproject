package com.illarien.costcotakehomeproject.repository.typeconverters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.illarien.costcotakehomeproject.model.Restaurant

class ListConverters {
    @TypeConverter
    fun toStringList(data: String?): List<String> =
        when (data) {
            null -> listOf()
            else -> Gson().fromJson(data, object : TypeToken<List<String>>() {}.type)
        }

    @TypeConverter
    fun listOfStringsToGsonString(data: List<String>): String = Gson().toJson(data)

    @TypeConverter
    fun toRestaurantList(data: String?): List<Restaurant> =
        when (data) {
            null -> listOf()
            else -> Gson().fromJson(data, object : TypeToken<List<Restaurant>>() {}.type)
        }

    @TypeConverter
    fun listOfRestaurantsToGsonString(data: List<Restaurant>): String = Gson().toJson(data)
}