package com.illarien.costcotakehomeproject.repository

import androidx.lifecycle.LiveData
import com.illarien.costcotakehomeproject.model.Cities
import com.illarien.costcotakehomeproject.model.Restaurants
import com.illarien.costcotakehomeproject.repository.api.OpenTableApi
import com.illarien.costcotakehomeproject.repository.dao.CityDao
import com.illarien.costcotakehomeproject.repository.dao.RestaurantDao
import com.illarien.costcotakehomeproject.ui.main.MainState
import com.illarien.costcotakehomeproject.utils.attach
import com.illarien.costcotakehomeproject.utils.invokeRx
import io.reactivex.MaybeObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

interface OpenTableRepository {
    fun getCities(): LiveData<Cities>
    fun getRestaurantsByCity(city: String): LiveData<Restaurants>
    fun getAllRestaurants(): LiveData<Restaurants>
    fun fetchCities(callback: (state: MainState) -> Unit)
    fun fetchRestaurantByCity(city: String, callback: (state: MainState) -> Unit)
}

class OpenTableRepositoryImpl @Inject constructor(
    private val openTableApi: OpenTableApi,
    private val cityDao: CityDao,
    private val restaurantDao: RestaurantDao
) : OpenTableRepository{

    override fun getCities(): LiveData<Cities> = cityDao.getCities(1)
    override fun getRestaurantsByCity(city: String) = restaurantDao.getRestaurantsByCity(city)
    override fun getAllRestaurants() = restaurantDao.getAllRestaurants()

    override fun fetchCities(callback: (state: MainState) -> Unit) {
        openTableApi.fetchCities()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : MaybeObserver<Cities> {
                override fun onSubscribe(d: Disposable) {
                    d.attach()
                }

                override fun onSuccess(cities: Cities) {
                    invokeRx { cityDao.insertCities(cities) }
                    callback(MainState.OnSuccess)
                }

                override fun onError(e: Throwable) {
                    Timber.v("Network request error ${e.message}")
                    callback(MainState.OnError(e))
                }

                override fun onComplete() = Unit
            })
    }

    override fun fetchRestaurantByCity(city: String, callback: (state: MainState) -> Unit) {
        openTableApi.fetchRestaurantByCity(city)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : MaybeObserver<Restaurants> {
                override fun onSubscribe(d: Disposable) {
                    d.attach()
                }

                override fun onSuccess(restaurants: Restaurants) {
                    invokeRx {
                        restaurantDao.clearRestaurants()
                        restaurantDao.insertRestaurant(restaurants.copy(city = city))
                    }
                    callback(MainState.OnSuccess)
                }

                override fun onError(e: Throwable) {
                    Timber.v("Network request error ${e.message}")
                    callback(MainState.OnError(e))
                }

                override fun onComplete() = Unit
            })
    }
}