package com.illarien.costcotakehomeproject.repository

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.illarien.costcotakehomeproject.model.Cities
import com.illarien.costcotakehomeproject.model.Restaurants
import com.illarien.costcotakehomeproject.repository.dao.CityDao
import com.illarien.costcotakehomeproject.repository.dao.RestaurantDao
import com.illarien.costcotakehomeproject.repository.typeconverters.ListConverters

@Database(entities = [Cities::class, Restaurants::class], version = 4)
@TypeConverters(ListConverters::class)
abstract class OpenTableDataBase : RoomDatabase() {
    abstract fun cityDao(): CityDao
    abstract fun restaurantDao(): RestaurantDao
}