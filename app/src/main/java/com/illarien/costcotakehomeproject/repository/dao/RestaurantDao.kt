package com.illarien.costcotakehomeproject.repository.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.illarien.costcotakehomeproject.model.Restaurants

@Dao
interface RestaurantDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertRestaurant(restaurant: Restaurants)

    @Query("SELECT * FROM Restaurants WHERE city =:city")
    fun getRestaurantsByCity(city: String): LiveData<Restaurants>

    @Query("SELECT * FROM Restaurants")
    fun getAllRestaurants(): LiveData<Restaurants>

    @Query("SELECT COUNT(*) FROM Restaurants")
    fun getRestaurantsCount(): Int

    @Query("DELETE FROM Restaurants")
    fun clearRestaurants()
}