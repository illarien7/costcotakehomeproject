package com.illarien.costcotakehomeproject.repository.api

import com.illarien.costcotakehomeproject.model.Cities
import com.illarien.costcotakehomeproject.model.Restaurants
import io.reactivex.Maybe
import retrofit2.http.GET
import retrofit2.http.Query

interface OpenTableApi {
    @GET("cities")
    fun fetchCities(): Maybe<Cities>

    @GET("restaurants")
    fun fetchRestaurantByCity(@Query("city") city: String): Maybe<Restaurants>
}