package com.illarien.costcotakehomeproject.repository.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.illarien.costcotakehomeproject.model.Cities

@Dao
interface CityDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCities(cities: Cities)

    @Query("SELECT * FROM Cities WHERE id =:id")
    fun getCities(id: Int): LiveData<Cities>
}