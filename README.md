# README #

### Costco Take Home coding Challenge ###

The app build in Kotlin based on MVVM + LiveData architecture with Dagger 2 injections and
    follows best practices from the Android Architecture Guide:
    https://developer.android.com/jetpack/docs/guide

    The main idea is to emmit different MainViewStates by merging different LiveData into
    MediatorLiveData in MainViewModel.

    The data flow:
    - fetch data from endpoint and put it into Room DB;
    - Subscribe to Room DB queries with using LiveData and update UI based on it.

    The list of third party libraries used:
    - Dagger2;
    - RxJava;
    - Retrofit, GSON;
    - Architecture Components: ViewModel, Room, LiveData;
    - Timber;
    - Picasso.;
    - Material search view.

    Complience with Requirements:
     Functional Requirements:
     + Provide an interface for inputting search terms by ‘city’.
     + Display 25 results for the given search term, including a thumbnail of the image and the title.
     + Selecting a thumbnail or title displays the full photo & descriptions.
     + Provide a way to return to the search results.
     + Provide a way to search for another term.

    Technical Requirements:
     + Make all calls to the OpenTable REST API.
     + Consume all API content in JSON.
     + Use your own code to communicate with the OpenTable API. (Use of other third party libraries is okay)
     + At least two files must be written in Java and at least two in Kotlin.

    Extra Mile (not required):
     + Save prior search terms and present them as quick search options.
     - Not done: Page results. (allowing more than the initial 25 to be displayed)
     + Sensible error handling and testing.
     - Not done, OpenTable API does not return a proper images, just a stub image. Ability to bookmark and view images offline.


    Tested on Pixel XL (API 26, emulator), WQVGA (API 26, low density screen),
    Samsung Galaxy S9, Samsung Galaxy S7.